import java.io.File;

/**
 * Created by benjaminmuldrow on 5/25/16.
 *
 * This is a utility file that is used for parsing data from files
 * into String Arrays which can be read as data for the TableView
 *
 */
public class FileParser {

    /**
     * Parse CSV File
     * @param file CSV file to read data from
     * @return String array of values
     */
    public static String [] parseCsv(File file) {
        return null;
    }

    /**
     * Parse TSV File
     * @param file TSV file to read data from
     * @return String array of values
     */
    public static String [] parseTsv(File file) {
        return null;
    }

    /**
     * Parse TXT File given a delimiter
     * @param file txt file to read
     * @param delimiter data delimiter
     * @return String array of values
     */
    public static String [] parseTxt(File file, String delimiter) {
        return null;
    }

}
