import javafx.scene.control.*;

/**
 * Created by sbunce on 5/30/2016.
 */

public class MainMenuBar extends MenuBar {
    private MenuBar menuBar = new MenuBar();

    public MainMenuBar(){
        super();
        this.initialize();
    }

    public void initialize(){
        // File Menu
        Menu fileMenu = new Menu("File");
        MenuItem newFile = new MenuItem("New");
        MenuItem openFile = new MenuItem("Open");
        MenuItem closeFile = new MenuItem("Close");
        MenuItem importData = new MenuItem("Import Data");
        SeparatorMenuItem separator = new SeparatorMenuItem();
        fileMenu.getItems()
                .addAll(newFile,
                        openFile,
                        closeFile,
                        importData);

        // Edit Menu
        Menu editMenu = new Menu("Edit");

        // View Menu
        Menu viewMenu = new Menu("View");

        // Add menus to menuBar
        menuBar.getMenus()
                .addAll(fileMenu,
                        editMenu,
                        viewMenu);
    }

    //Returns compatible type to be added to main window
    public MenuBar getMenuBar(){
        return menuBar;
    }
}
