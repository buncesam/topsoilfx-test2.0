import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        MainMenuBar menuBar = new MainMenuBar();
        MainButtonsBar buttonBar = new MainButtonsBar();
        Scene scene = new Scene(new VBox(), 750, 750);

        ((VBox) scene.getRoot()).getChildren().addAll(menuBar.getMenuBar(), buttonBar.getButtons());
        primaryStage.setTitle("Topsoil Test");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}