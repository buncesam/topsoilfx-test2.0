import javafx.beans.property.StringProperty;

/**
 * Created by benjaminmuldrow on 5/25/16.
 */
public class DataEntry {

    public StringProperty numericalValue;
    public StringProperty category;
}
